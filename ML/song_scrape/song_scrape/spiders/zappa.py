import scrapy
from song_scrape.items import SongScrapeItem


class QuotesSpider(scrapy.Spider):
    name = "zappa"

    def start_requests(self):
        urls = [
            'http://www.metrolyrics.com/frank-zappa-lyrics.html',
            'http://www.metrolyrics.com/frank-zappa-alpage-2.html',
            'http://www.metrolyrics.com/frank-zappa-alpage-3.html',
            'http://www.metrolyrics.com/frank-zappa-alpage-4.html',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        songs = response.xpath("//table[@class='songs-table compact']//td/a")
        for song in songs:
            song_name = song.xpath("@title").extract_first()
            song_link = song.xpath("@href").extract_first() 
            request_song = scrapy.Request(url=song_link, callback=self.parse_song)
            request_song.meta['song_name'] = song_name
            yield request_song

    def parse_song(self, response):
        band = "Frank Zappa"
        song = response.meta.get('song_name')
        lyrics_split = response.xpath("//p[@class='verse']//text()")

        lyrics_full = ""
        for verse in lyrics_split: 
            lyrics_full += verse.extract() + " "
        yield SongScrapeItem(band=band,
                             song=song,
                             lyrics=lyrics_full
                             )