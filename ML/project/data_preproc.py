import pandas as pd

pink = pd.read_csv("pink.csv")
yes = pd.read_csv("yes.csv")
tool = pd.read_csv("tool.csv")
zappa = pd.read_csv("zappa.csv")


yes.isnull().sum()
pink.isnull().sum()
tool.isnull().sum()
zappa.isnull().sum()

del pink["song"]
del yes["song"]
del tool["song"]
del zappa["song"]

import matplotlib.pyplot as plt
import seaborn as sns


full_data = [pink, yes, tool]
df = pd.concat(full_data)


sns.countplot(data=df, x='band');


all_words = df['lyrics'].str.split(expand=True).unstack().value_counts()
all_words.head(15)
all_words.tail(15)


