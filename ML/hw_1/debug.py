import sys

import sklearn
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns


train = pd.read_csv('train.csv', index_col=['Id'])
test = pd.read_csv('test.csv', index_col=['Id'])


print('missing column:', set(train.columns) - set(test.columns))
print(train.LandContour.value_counts())
print(train.Electrical.value_counts())
print(train.Alley.value_counts())
print(train.FullBath.value_counts())

from sklearn.model_selection import train_test_split

train_no_sale_price = train.drop(labels=['SalePrice'], axis=1)
train_sale_price = train['SalePrice']

x_train, x_test, y_train, y_test = train_test_split(train_no_sale_price, 
                                                    train_sale_price, 
                                                    test_size=0.3, 
                                                    random_state=4330)

for df in [x_train, x_test, y_train, y_test]:
    print(df.shape)
    
from sklearn.linear_model import LinearRegression

regressor = LinearRegression()
regressor.fit(x_train, y_train)


print((x_train.values == 'Abnorml').sum())

col_idx = pd.np.argmax(x_train.values == 'Abnorml', axis=1).max()

x_train.iloc[:, col_idx].value_counts()


x_train.describe()

pd.options.display.max_columns = 36
pd.options.display.max_rows = 10

numeric_column_names = x_train.describe().columns
print(numeric_column_names)


regressor.fit(x_train[numeric_column_names], y_train)


x_train[numeric_column_names].isnull().sum().sort_values()


x_train[['LotFrontage','GarageYrBlt', 'MasVnrArea']].hist();


x_train[numeric_column_names] = x_train[numeric_column_names].fillna(0)

x_train[numeric_column_names].isnull().sum(axis=0).value_counts()


regressor.fit(x_train[numeric_column_names], y_train)

regressor.score(x_train[numeric_column_names], y_train)


predictions = regressor.predict(x_train[numeric_column_names])

print(predictions[:8])

sns.distplot(predictions);




differences = (predictions - y_train).round(0)
print(differences[:8])

1 - predictions / y_train



plt.figure(figsize=(12,12))
sns.regplot(y_train, predictions)
plt.grid(True)
plt.show()



x_test[numeric_column_names].isnull().sum().sort_values(ascending=False)[:4]

x_test[numeric_column_names] = x_test[numeric_column_names].fillna(0)


predictions_test = regressor.predict(x_test[numeric_column_names])
plt.figure(figsize=(12,12))
sns.regplot(y_test, predictions_test)
plt.grid(True)
plt.show()


print("score for test:", regressor.score(x_test[numeric_column_names], y_test))


y_test - predictions_test

deviation = 1 - predictions_test / y_test
deviation[:8]

plt.figure(figsize=(12,8))
barplot = sns.barplot(x=numeric_column_names, y=regressor.coef_, orient='vertical')
plt.setp(barplot.get_xticklabels(), rotation=90); plt.grid(True);


print(regressor.intercept_)
print(regressor.coef_)


print(regressor.intercept_, " + ",)
list(zip(regressor.coef_, "*"*len(regressor.coef_), numeric_column_names, ))


pd.options.display.max_rows = 10
train.dtypes


categorical_columns = train.dtypes[train.dtypes == 'object'].index
print(categorical_columns)


from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
enc = OneHotEncoder()


train = train.fillna("")
encoders = {col: LabelEncoder().fit(train[col]) for col in categorical_columns}

print(encoders['MSZoning'].classes_)
print(encoders['Street'].classes_)


def encode_categorical(data, columns, encoders):
    data = data.fillna("")
    return pd.DataFrame({col: encoders[col].transform(data[col]) for col in columns},
                        index = data.index)

train_encoded = encode_categorical(train, categorical_columns, encoders)

pd.options.display.max_columns=12
train_encoded.head(8)



one_hot_encoder = OneHotEncoder().fit(train_encoded)

print(one_hot_encoder.transform(train_encoded[:10]).todense())
print(one_hot_encoder.transform(train_encoded).shape)


from sklearn.linear_model import LinearRegression 

one_hot_x_train = one_hot_encoder.transform(encode_categorical(x_train[categorical_columns], categorical_columns, encoders))

cat_regression = LinearRegression().fit(one_hot_x_train, y_train)
print(cat_regression.score(one_hot_x_train, y_train))

one_hot_x_test = one_hot_encoder.transform(encode_categorical(x_test[categorical_columns], categorical_columns, encoders))
print(cat_regression.score(one_hot_x_test, y_test))



x_train[numeric_column_names].shape, one_hot_x_train.shape


new_x_train = pd.np.concatenate([one_hot_x_train.todense(), x_train[numeric_column_names]], axis=1)
new_x_test = pd.np.concatenate([one_hot_x_test.todense(), x_test[numeric_column_names]], axis=1)
print(new_x_train.shape, new_x_test.shape)



all_data_lr = LinearRegression().fit(new_x_train, y_train)
print(all_data_lr.score(new_x_train, y_train))
print(all_data_lr.score(new_x_test, y_test))


from sklearn.linear_model import Ridge

alpha = [0.01, 0.1, 1, 10, 100]

for a in alpha:
    all_data_lr = Ridge(alpha = a).fit(new_x_train, y_train)
    print('alpha:', a)
    print(all_data_lr.score(new_x_train, y_train), all_data_lr.score(new_x_test, y_test))
    print()
