import sys
import numpy as np
import sklearn
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns


def encode_categorical(data, columns, encoders):
    data = data.fillna("")
    return pd.DataFrame({col: encoders[col].transform(data[col]) for col in columns},
                        index = data.index)

train = pd.read_csv('train.csv', index_col=['Id'])
test = pd.read_csv('test.csv', index_col=['Id'])

from sklearn.model_selection import train_test_split
train_no_sale_price = train.drop(labels=['SalePrice'], axis=1)
train_sale_price = train['SalePrice']

x_train, x_test, y_train, y_test = train_test_split(train_no_sale_price, 
                                                    train_sale_price, 
                                                    test_size=0.3, 
                                                    random_state=4330)

from sklearn.preprocessing import LabelEncoder
le = LabelEncoder()

from sklearn.preprocessing import OneHotEncoder
enc = OneHotEncoder()

# list of str based cols
categorical_columns = train.dtypes[train.dtypes == 'object'].index

# list of numeric based features
numeric_column_names = x_train.describe().columns

x_train[numeric_column_names] = x_train[numeric_column_names].fillna(0)

# TODO make predictions for features rather than null-ing them

# Dict of col_name => encoder_instance
train = train.fillna("")
encoders = {col: LabelEncoder().fit(train[col]) for col in categorical_columns}



# df of numerized str cols
train_encoded = encode_categorical(train, categorical_columns, encoders)

one_hot_encoder = OneHotEncoder().fit(train_encoded)
one_hot_x_train = one_hot_encoder.transform(encode_categorical(x_train[categorical_columns], categorical_columns, encoders))
one_hot_x_test = one_hot_encoder.transform(encode_categorical(x_test[categorical_columns],
                                                              categorical_columns,
                                                              encoders
                                                              ))

# numeric dfs
num_x_train = pd.np.concatenate([one_hot_x_train.todense(), x_train[numeric_column_names]], axis=1)
num_x_test = pd.np.concatenate([one_hot_x_test.todense(), x_test[numeric_column_names]], axis=1)

# Check which features have normal distribution
from scipy import stats
list_of_sw_test = []
for col in num_x_train.T:
    list_of_sw_test.append(stats.shapiro(col))
    
count_col = 0
for el in list_of_sw_test:
    if el[1] > 0.1:
        print("Feat {} is not Normal Distr with val: {}".format(count_col, el))
    count_col +=1
    
# correlation matrix 
full_data_matrix = pd.np.concatenate([num_x_train, y_train[:, None]], axis=1)    
corr_matrix = np.corrcoef(full_data_matrix.T)

label_correlation = abs(corr_matrix[:, 304])
label_correlation.sort()
 
# Imame nqkolko nan feature-a, kakvo da gi pravim ?!
corr_val = 0.35
list_big_corr_features = []
count = 0
for el in abs(corr_matrix[:, 304]):
    if(el > corr_val and el < 1):
        list_big_corr_features.append((count,el))
    count +=1


# Top Feature is 271
count = 0
top_feature_corr = []
for el in corr_matrix[:,271]:
    if(el > 0.2 and el < 1):
        top_feature_corr.append((count,el))
    count +=1
    
# In this experiment we see that the top features are highly correlated with the top feature
for el in list_big_corr_features:
    if(abs(corr_matrix[el[0],271]) < 0.3):
        print(el)
    

    data = data.fillna("")


from sklearn.linear_model import LinearRegression 
cat_regression = LinearRegression().fit(one_hot_x_train, y_train)


new_x_train = pd.np.concatenate([one_hot_x_train.todense(), x_train[numeric_column_names]], axis=1)
new_x_train = pd.np.concatenate([one_hot_x_train.todense(), x_train[numeric_column_names]], axis=1)
new_x_test = pd.np.concatenate([one_hot_x_test.todense(), x_test[numeric_column_names]], axis=1)
print(new_x_train.shape, new_x_test.shape)
new_x_train[new_x_train is np.nan] = -9999
np.nan_to_num(new_x_train)



num_x_train.shape
num_x_test.shape

index = ['Col'+str(i) for i in range(0, num_x_train.shape[1])]
num_x_train.shape[1]

tmp_df = pd.DataFrame(num_x_train)


