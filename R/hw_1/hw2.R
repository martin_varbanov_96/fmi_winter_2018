# Hw input model, df -> output - p-value
get_p_val_t_test <- function(input_model, input_df, input_foreign_SSE){
  y=input_df$sr
  
  # calc SST, SSR, SSE
  SST = sum((y-mean(y))^2)
  SSE = sum((y-input_model$fit)^2)
  SSR = sum((mean(y)-input_model$fit)^2)
  
  curr_model_SSE = sum(input_model$res^2)
  
  # get nxp and deg of freedom
  p=length(input_model$coefficients)
  n=dim(savings)[1]
  deg_freedom = n-p
  
  # Get the t-statistic kvantil
  kvantil = (curr_model_SSE-input_foreign_SSE)/(input_foreign_SSE/deg_freedom)
  kvantil_sqrt = sqrt(kvantil)

  
  # return p-value na modela
  return(2*(1-pt(kvantil_sqrt,deg_freedom)))
}
 
g2 <- lm(sr ~ pop75 + dpi + ddpi, data=savings)

reslt = get_p_val_t_test(input_model = g2, input_df = savings, input_foreign_SSE = a)
reslt
