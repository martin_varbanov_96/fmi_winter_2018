
# Homework 1
# Martin Nikolaev Varbanov

## Task 1:

    - implement a function, which returns p-value, given a model and a DataFrame


```R
# Hw input model, df -> output - p-value
get_p_val <- function(input_model, input_df){
  
  y=input_df$sr
  
  # tuka smqtame R^2
  cor(savings$sr, m1$fit)^2
  
  # calc SST
  SST = sum((y-mean(y))^2)
  
  # calc SSE
  SSE = sum((y-input_model$fit)^2)
  
  # calc SSR
  SSR = sum((mean(y)-input_model$fit)^2)
  
  # calculate F = (SST-SSE)/df / SSTE/df
  p=length(input_model$coefficients)
  n=dim(savings)[1]
  F_stat = ((SST-SSE)/(p-1)) / (SSE/(n-p)) 
  
  # return p-value na modela
  return(1-pf(F_stat, p-1, n-p))
}

library('faraway')
data(savings)
summary(savings)

# Model
m1 <- lm(sr ~ ., data=savings)

# use function
get_p_val(m1, savings)

```


           sr             pop15           pop75            dpi         
     Min.   : 0.600   Min.   :21.44   Min.   :0.560   Min.   :  88.94  
     1st Qu.: 6.970   1st Qu.:26.21   1st Qu.:1.125   1st Qu.: 288.21  
     Median :10.510   Median :32.58   Median :2.175   Median : 695.66  
     Mean   : 9.671   Mean   :35.09   Mean   :2.293   Mean   :1106.76  
     3rd Qu.:12.617   3rd Qu.:44.06   3rd Qu.:3.325   3rd Qu.:1795.62  
     Max.   :21.100   Max.   :47.64   Max.   :4.700   Max.   :4001.89  
          ddpi       
     Min.   : 0.220  
     1st Qu.: 2.002  
     Median : 3.000  
     Mean   : 3.758  
     3rd Qu.: 4.478  
     Max.   :16.710  



0.000790377938795217


## Task 2:

        - Implement a t statistic for 2 models, comparing if a given feature is significant


```R
# Hw input model, df -> output - p-value
get_p_val_t_test <- function(input_model, input_df, input_foreign_SSE){
  y=input_df$sr
  
  # calc SST, SSR, SSE
  SST = sum((y-mean(y))^2)
  SSE = sum((y-input_model$fit)^2)
  SSR = sum((mean(y)-input_model$fit)^2)
  
  curr_model_SSE = sum(input_model$res^2)
  
  # get nxp and deg of freedom
  p=length(input_model$coefficients)
  n=dim(savings)[1]
  deg_freedom = n-p
  
  # Get the t-statistic kvantil
  kvantil = (curr_model_SSE-input_foreign_SSE)/(input_foreign_SSE/deg_freedom)
  kvantil_sqrt = sqrt(kvantil)

  
  # return p-value na modela
  return(2*(1-pt(kvantil_sqrt,deg_freedom)))
}
 
g2 <- lm(sr ~ pop75 + dpi + ddpi, data=savings)

reslt = get_p_val_t_test(input_model = g2, input_df = savings, input_foreign_SSE = a)
reslt
```


0.00232858347887155

